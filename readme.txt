=== WpCF7 Autonomous Captcha ===

Contributors: cyrille37
Donate link: https://contactform7.com/donate/
Tags: Contact Form 7, Captcha
Requires at least: 6
Tested up to: 6.5.2
Stable tag: 1.1
Requires PHP: 7.4
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Ce plugin Wordpress fourni un captcha tout simple pour le gestionnaire de formulaire "Contact Form 7".

== Description ==

La différence avec "Really Simple CAPTCHA" est qu'il ne gènère aucune activité sur le disque ni dans la session.
L'image est transportée dans le tag "img" avec un URI `data:image/png;base64,...`
et la réponse au défi est stockée chiffrée dans un champs du formulaire.

il existe aussi:

* https://wordpress.org/plugins/contact-form-7-image-captcha/ par KC Computing

== Utilisation ==

En respectant la [syntaxe de Contact Form 7](https://contactform7.com/tag-syntax/).

```
[autonomous_captcha captcha]
```

Il est possible de restreindre l'accès au formulaire à des pays (les IPs privées ne sont pas filtrées).
Cette option est dépendante de l'installation et l'activation d'un des plugins:

* [wp-statistics](https://wordpress.org/plugins/wp-statistics/)
* [wordfence](https://wordpress.org/plugins/wordfence/)

Par exemple seules les IPs identifiées en Espagne ou en France pourront valider le formulaire :

```
[autonomous_captcha captcha country:ES|FR]
```
