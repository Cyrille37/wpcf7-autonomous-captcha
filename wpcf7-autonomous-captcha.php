<?php
/*
Plugin Name: ContactForm7 Autnomous Captcha
Plugin URI:
Description: "ContactForm7 Autnomous Captcha" is a CAPTCHA module like "Really Simple CAPTCHA" but without any file or session access, it's really autonomous (aka standalone).
Author: Cyrille37
Author URI: https://framagit.org/Cyrille37
Text Domain: wpcf7-autonomous-captcha
Version: 1.1
*/

/**
 * WPCF7 developper documentation:
 * - Adding a custom form-tag: https://contactform7.com/2015/01/10/adding-a-custom-form-tag
 * - Using values from a form-tag: https://contactform7.com/2015/02/27/using-values-from-a-form-tag
 * - Custom validation: https://contactform7.com/2015/03/28/custom-validation/#more-13626
 * - Dom event
 * - Some code:
 * - https://plugins.trac.wordpress.org/browser/contact-form-7-simple-recaptcha/trunk/contact-form-7-simple-recaptcha.php
 *
 */
class Wpcf7AutonomousCaptcha
{
    const TEXT_DOMAIN = 'wpcf7-autonomous-captcha';
    const TAG_NAME = 'autonomous_captcha' ;

    protected $image_config = [
        'size' => [130, 36],
        'bg-color' => [255, 255, 255],
        'fg-color' => [0, 0, 0],
        'font-size' => 18,
        'font-span' => 18,
        'font-x' => 8,
        'font-y' => 25,
        'chars' => 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789',
        'captcha_length' => 5,
    ];

    public function __construct()
    {
        if ($this->ignoreRequest())
            return;

        add_action('wpcf7_init', [$this, 'wpcf7_init']);
    }

    protected function ignoreRequest()
    {
        if (defined('DOING_CRON') && DOING_CRON) {
            // No Cron stuff with this plugin
            return true;
        }

        if (defined('DOING_AJAX') && DOING_AJAX && isset($_REQUEST['action']) ) {
            // No Ajax with this plugin
            switch ($_REQUEST['action']) {
                case 'heartbeat':
                case 'closed-postboxes':
                case 'health-check-site-status-result':
                case 'oembed-cache':
                default:
                    return true;
            }
        }

        /*
        error_log(__METHOD__ . ' CONSTANTS:' . var_export(get_defined_constants(true)['user'], true));
        error_log(__METHOD__ . ' REQUEST:' . var_export($_REQUEST, true));
        */

        if( defined('WP_DEBUG') && WP_DEBUG )
        {
            $uri = $_SERVER['REQUEST_URI'];
            if( strlen($uri)>5 && strpos($uri, '.map', -5 ) !== false )
                return true ;
        }

        return false;
    }

    public function wpcf7_init()
    {
        wpcf7_add_form_tag(self::TAG_NAME, [$this, 'wpcf7_formtag_handler_autonomous_captcha'], [
            'name-attr' => true,
            'do-not-store' => true,
            'not-for-mail' => true,
        ]);
        add_filter('wpcf7_validate_autonomous_captcha', [$this, 'wpcf7_validate_autonomous_captcha'], 20, 2);

        // Ajax stuff
        //add_filter('wpcf7_refill_response', [$this,'wpcf7_ajax_refill_response'], 10, 1 );
        //add_filter('wpcf7_feedback_response', [$this,'wpcf7_ajax_feedback_response'], 10, 1 );

        //add_filter( 'wpcf7_spam', [$this,'wpcf7_spam'], 10, 2 );

    }

    /**
     * Build the input control
     */
    public function wpcf7_formtag_handler_autonomous_captcha($tag)
    {
        //error_log(__METHOD__);
        //error_log( var_export($tag,true));

        /* Trop galère, il faudrait recalculer plein d'éléments graphique :(
        if ( $tag->has_option('captcha-length') )
        {
            $captcha_length = $tag->get_option('captcha-length','int',true);
            $this->image_config['captcha_length'] = $captcha_length > 0 ? $captcha_length : $this->image_config['captcha_length'] ;
        }
        */

        $validation_error = wpcf7_get_validation_error($tag->name);
        $class = wpcf7_form_controls_class($tag->type);
        if ($validation_error) {
            $class .= ' wpcf7-not-valid';
        }

        $atts = [];
        $atts['size'] = 2 * $this->image_config['captcha_length'];
        $atts['maxlength'] = $this->image_config['captcha_length'];
        $atts['minlength'] = $this->image_config['captcha_length'];
        //$atts['country'] = '' ;

        $atts['class'] = $tag->get_class_option($class);
        $atts['id'] = $tag->get_id_option();
        $atts['tabindex'] = $tag->get_option('tabindex', 'signed_int', true);
        $atts['autocomplete'] = 'off';

        if ($validation_error) {
            $atts['aria-invalid'] = 'true';
            $atts['aria-describedby'] = wpcf7_get_validation_error_reference(
                $tag->name
            );
        } else {
            $atts['aria-invalid'] = 'false';
        }

        $value = (string) reset($tag->values);

        if (wpcf7_is_posted()) {
            $value = '';
        }

        $country_restriction = $this->restrict_country( $tag );

        $atts['value'] = $value;
        $atts['type'] = 'text';
        $atts['name'] = \esc_attr($tag->name);

        $word = $this->generate_word($this->image_config['captcha_length']);

        $word_crypted = $this->encrypt($word);
        $answer = '<input type="hidden" name="' . $atts['name'] . '-answer' . '" value="' . $word_crypted . '"/>';
        $img = '<img src="data:image/jpeg;base64,' . $this->generate_image($word) . '" />';

        $html = '';
        $html .= $img;
        $html .= $answer;
        $html .= sprintf(
            '<span class="wpcf7-form-control-wrap" data-name="%1$s"><input %2$s />%3$s</span>',
            $atts['name'],
            \wpcf7_format_atts($atts),
            $validation_error
        );
        if( $country_restriction )
        {
            //$html.='<span class="wpcf7-autonomous-captcha-country-restriction">A country restriction is applied.</span>' ;
        }

        return $html;
    }

    public function wpcf7_validate_autonomous_captcha($result, $tag)
    {
        //error_log( __METHOD__.' '.var_export($tag, true));

        $current = $_POST[$tag->name];
        $expected = $this->decrypt($_POST[$tag->name . '-answer']);
        if ($current != $expected) {
            $result->invalidate($tag, __('Les caractères ne correspondent pas.', self::TEXT_DOMAIN));
        }

        if( $this->restrict_country( $tag ) )
        {
            $result->invalidate($tag, __('Votre pays de connexion Internet n’est pas autorisé à utiliser ce formulaire.', self::TEXT_DOMAIN));
        }

        return $result;
    }

    public function wpcf7_ajax_refill_response( $items )
    {
        error_log( __METHOD__.' '.var_export($items, true));
        // Couplé avec du JS CF7, pas de solution ici pour rafraichir le captcha.
        return $items;
    }

    public function wpcf7_ajax_feedback_response( $items )
    {
        error_log( __METHOD__.' '.var_export($items, true));
        /*
        Wpcf7AutonomousCaptcha::wpcf7_ajax_feedback_response array (
            'contact_form_id' => 7368,
            'status' => 'mail_sent',
            'message' => 'Merci pour votre message. Il a été envoyé.',
            'posted_data_hash' => '352be6d172fd785df20d46a1bfae1e92',
            'into' => '#wpcf7-f7368-p10813-o1',
            'invalid_fields' => array (),
        )
        */
        // Couplé avec du JS CF7, pas de solution ici pour rafraichir le captcha.
        return $items;
    }

    /**
     * Restrict IP if GeoIp plugins aware are active
     * and is not a private ip.
     */
    protected function restrict_country( $tag )
    {
        if ( ! $tag->has_option('country') )
            return false ;

        $countries = $tag->get_option('country','',true) ;
        if( empty($countries) && is_string($countries) )
            return false ;
        $countries = explode( '|', $countries );

        $dir = plugin_dir_path( __DIR__ );

        if( class_exists( '\WP_STATISTICS\GeoIP', true) )
        {
            $ip = \WP_STATISTICS\IP::getIP();
            //error_log(__METHOD__ . ' provider WP_STATISTICS ip='.$ip);
            $ip = '37.65.65.95' ;
            if( $ip == \WP_STATISTICS\IP::$default_ip )
                return false ;
            $res = \WP_STATISTICS\GeoIP::getCountry($ip);
            //error_log(__METHOD__ . ' country: '. \var_export($res,true).' in '.var_export($countries,true));
            if( in_array($res, $countries) )
                return false ;
            return true ;
        }

        if( class_exists( '\wfIpLocator', true) /*|| file_exists( $dir.'wordfence/lib/wfIpLocator.php' )*/ )
        {
            //error_log(__METHOD__ . ' provider wfIpLocator');
            require_once( $dir.'wordfence/lib/wfIpLocator.php');
            require_once( $dir.'wordfence/lib/wfUtils.php');
            $ip = \wfUtils::getIP();
            //$ip = '37.65.65.95' ;
            //error_log(__METHOD__ . ' ip: '. \var_export($ip,true));
            if( \wfUtils::isPrivateAddress($ip) )
                return false ;
            $res = \wfIpLocator::getInstance()->getCountryCode($ip);
            //error_log(__METHOD__ . ' country: '. \var_export($res,true));
            if( in_array($res, $countries) )
                return false ;
            return true ;
        }

        return false ;
    }

    protected function generate_word($captcha_length)
    {
        $word = '';
        $chars_count = strlen($this->image_config['chars']);
        $chars = &$this->image_config['chars'];
        for ($i = 0; $i < $captcha_length; $i++) {
            $word .= $chars[mt_rand(0, $chars_count - 1)];
        }
        return $word;
    }

    protected function generate_image($word)
    {
        $cfg = &$this->image_config;
        $im = imagecreatetruecolor(
            $this->image_config['size'][0],
            $this->image_config['size'][1]
        );
        $bg = imagecolorallocate($im, $cfg['bg-color'][0], $cfg['bg-color'][1], $cfg['bg-color'][2]);
        $fg = imagecolorallocate($im, $cfg['fg-color'][0], $cfg['fg-color'][1], $cfg['fg-color'][2]);
        imagefill($im, 0, 0, $bg);

        $font = __DIR__ . '/gentium-book-plus-v1-latin-regular.ttf';

        $x = $cfg['font-x'];
        $y = $cfg['font-y'];
        for ($i = 0; $i < strlen($word); $i++) {
            $font_size = mt_rand($cfg['font-size'] - 1, $cfg['font-size'] + 1);
            imagettftext(
                $im,
                $font_size,
                mt_rand(-12, 12),
                $x,
                $y + mt_rand(-2, 2),
                $fg,
                $font,
                $word[$i]
            );
            $x += $cfg['font-span'];
        }

        ob_start();
        imagejpeg($im);
        $im_data = ob_get_contents();
        ob_end_clean();

        return base64_encode($im_data);
    }

    protected function encrypt($message, $key = NONCE_KEY, $encode = true, $method = 'aes-128-ctr')
    {
        $nonceSize = openssl_cipher_iv_length($method);
        $nonce = openssl_random_pseudo_bytes($nonceSize);

        $ciphertext = openssl_encrypt(
            $message,
            $method,
            $key,
            OPENSSL_RAW_DATA,
            $nonce
        );

        // Now let's pack the IV and the ciphertext together
        // Naively, we can just concatenate
        if ($encode) {
            return base64_encode($nonce . $ciphertext);
        }
        return $nonce . $ciphertext;
    }

    protected function decrypt($message, $key = NONCE_KEY, $encoded = true, $method = 'aes-128-ctr')
    {
        if ($encoded) {
            $message = base64_decode($message, true);
            if ($message === false) {
                throw new Exception('Encryption failure');
            }
        }

        $nonceSize = openssl_cipher_iv_length($method);
        $nonce = mb_substr($message, 0, $nonceSize, '8bit');
        $ciphertext = mb_substr($message, $nonceSize, null, '8bit');

        $plaintext = openssl_decrypt(
            $ciphertext,
            $method,
            $key,
            OPENSSL_RAW_DATA,
            $nonce
        );

        return $plaintext;
    }
}

new Wpcf7AutonomousCaptcha();
